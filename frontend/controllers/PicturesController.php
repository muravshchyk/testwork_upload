<?php

namespace frontend\controllers;

use common\models\UploadForm;
use Yii;
use common\models\Pictures;
use frontend\models\PicturesSearch;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PicturesController implements the CRUD actions for Pictures model.
 */
class PicturesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['create', 'update', 'delete'],
                'rules' => [

                    [
                        'allow'   => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Pictures models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new PicturesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pictures model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pictures model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        if (Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['site/login']));
        }

        $upload_model = new UploadForm();

        $user_pictures = new ActiveDataProvider([
            'query' => Pictures::find()->where(['user_id' => Yii::$app->user->getId()]),
        ]);

        if (Yii::$app->request->isPost) {
            $upload_model->imageFile = UploadedFile::getInstance($upload_model, 'imageFile');
            if ($upload_model->upload()) {
                Yii::$app->session->setFlash('success', 'File successfully uploaded');

            } else {
                Yii::$app->session->setFlash('success', 'Error downloading file');
            }
        }
        return $this->render('create', [
            'upload_model'  => $upload_model,
            'user_pictures' => $user_pictures

        ]);
    }

    /**
     * Updates an existing Pictures model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['site/login']));
        }

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pictures model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['site/login']));
        }

        $model = $this->findModel($id);
        if ($model) {
            unlink(Yii::getAlias('@frontend/web/uploads/' . $model->filename));
            $model->delete();
        }

        Yii::$app->session->setFlash('success', 'File successfully deleted ');
        return $this->redirect(Yii::$app->request->referrer);
    }


    /**
     * Finds the Pictures model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pictures the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pictures::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Action to rotate user images
     *
     * @param $id
     * @param int $angle
     */
    public function actionRotate($id, $angle = 90)
    {

        if (in_array($angle, [90, 180, 270])) {
            /**
             * @var $pictures Pictures
             */
            $pictures = Pictures::find()->where(['id' => $id])->one();

            if ($pictures->rotate($angle)) {
                Yii::$app->session->setFlash('success', 'Rotate image applied');
            } else {
                Yii::$app->session->setFlash('error', 'Rotate error');
                foreach ($pictures->getErrors() as $aErrors) {
                    foreach ($aErrors as $sError) {
                        Yii::$app->session->addFlash('error', $sError);
                    }
                }
            }
        } else {
            Yii::$app->session->addFlash('error', 'Not allowed angle');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }
}
