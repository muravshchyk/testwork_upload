<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $upload_model common\models\Pictures */
/* @var $user_pictures \yii\data\ActiveDataProvider */

$this->title                   = 'Upload Pictures';
$this->params['breadcrumbs'][] = ['label' => 'Pictures', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <?= $form->field($upload_model, 'imageFile')->fileInput(['accept' => 'image/*', 'data-buttonText' => 'Choose files to download']) ?>
    <button class='btn btn-success'>Upload</button>
    <?php ActiveForm::end() ?>
</div>

<div style="margin-top: 20px">
    <?= GridView::widget([
        'dataProvider' => $user_pictures,
        'layout'       => '{items}',
        'showOnEmpty'  => false,
        'emptyText'    => '',
        'columns'      => [
            ['label'  => 'PreView',
             'format' => 'raw',
             'value'  => function ($data) {
                 return "<img style='max-height: 100px' src='" . Yii::$app->urlManager->createAbsoluteUrl('uploads/' . $data->filename) . '?' . time() . "'>";
             }
            ],
            ['label'  => 'Actions',
             'format' => 'raw',
             'value'  => function ($data) {
                 return
                     Html::a('Rotate left', ['rotate', 'id' => $data->id, 'angle' => 270], ['class' => 'btn btn-primary']) . ' ' .
                     Html::a('Rotate 180', ['rotate', 'id' => $data->id, 'angle' => 180], ['class' => 'btn btn-primary']) . ' ' .
                     Html::a('Rotate right', ['rotate', 'id' => $data->id, 'angle' => 90], ['class' => 'btn btn-primary']) . ' ' .
                     Html::a('Delete', ['delete', 'id' => $data->id], ['class' => 'btn btn-danger', 'onclick' => 'return confirm("Are you sure you want to delete this item?") ? true : false;']);
             }
            ],
        ],
    ]);
    ?>
</div>



