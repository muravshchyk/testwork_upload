<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Example task to do on Yii2</h1>
        <p>Create a simple Yii2 Advanced site (with user login page) that will display a form and a table.
            The form will let user upload an image, the table will show list of uploaded images, and let user rotate and
            delete his images.

            * I don't care about UI design, just code design
            * You can use Yii Composer components if you want
            * make code and project CLEAN and READABLE (remove unneeded code, write well named functions and comments,
            make it easy to implement on a new computer)
            * When finished - please push to some git server (github/bitbucket) and share with me.

            1. Make sure u have a good structure
            2. Stick to MVC standards
            3. Use yii2-way maximally
            4. Make sure each user see its own items
            5. Before you send me the code - check yourself that you wrote good code. We tested over 100 candidates and
            first thing people fall on is bad code writing.</p>
        <iframe width="1100" height="600" src="https://www.youtube.com/embed/8ikb0dz0vDw" frameborder="0"
                allowfullscreen></iframe>
        <p><a class="btn btn-lg btn-success" href="https://bitbucket.org/muravshchyk/testwork_upload">Bitbucket</a></p>
    </div>
</div>
