<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pictures".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $filename
 * @property string $date_created
 * @property string $date_updated
 * @property string $original_filename
 */
class Pictures extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pictures';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'filename', 'date_created', 'date_updated', 'original_filename'], 'required'],
            [['user_id'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['filename'], 'string', 'max' => 64],
            [['original_filename'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'user_id'           => 'User ID',
            'filename'          => 'Filename',
            'date_created'      => 'Date Created',
            'date_updated'      => 'Date Updated',
            'original_filename' => 'Original Filename',
        ];
    }

    /**
     * @param int $angle
     *
     * @return bool
     */
    public function rotate($angle = 90)
    {
        $result       = false;
        $iQualityJpeg = 95;
        $sFilename    = 'uploads/' . $this->filename;

        if (!file_exists($sFilename)) {
            $this->addError('imageFiles', 'File not exist');
            return $result;
        }

        $aEtx = explode('.', $this->filename);
        $sEtx = array_pop($aEtx);

        switch ($sEtx) {
            case 'jpg':
                $imDoc = imagecreatefromjpeg($sFilename);
                break;
            case 'jpeg':
                $imDoc = imagecreatefromjpeg($sFilename);
                break;
            case 'png':
                $imDoc = imagecreatefrompng($sFilename);
                break;
            default:
                $this->addError('imageFiles', 'Wrong extension of image');

                return $result;
        }
        $imDoc = imagerotate($imDoc, $angle, 0);
        if ($imDoc) {
            switch ($sEtx) {
                case 'jpg':
                    $result = imagejpeg($imDoc, $sFilename, $iQualityJpeg);
                    break;
                case 'jpeg':
                    $result = imagejpeg($imDoc, $sFilename, $iQualityJpeg);
                    break;
                case 'png':
                    $result = imagepng($imDoc, $sFilename, 0, PNG_NO_FILTER);
                    break;
                default:
                    $this->addError('imageFiles', 'Error while save modified image');
                    $result = false;
                    break;
            }

            imagedestroy($imDoc);
        }

        return $result;
    }
}
