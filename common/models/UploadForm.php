<?php

namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;
use yii;
use yii\db\Expression;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            // 'checkExtensionByMimeType'=>false   - should be switched to true on production with following option in php.ini file
            // extension=php_fileinfo.dll
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg', 'checkExtensionByMimeType' => false,],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $filename = \Yii::$app->user->getId() . '_' . time() . '_' . rand(1, 9) . '.' . $this->imageFile->extension;
            if ($this->imageFile->saveAs('uploads/' . $filename)) {
                $pictures                    = new Pictures();
                $pictures->user_id           = Yii::$app->user->getId();
                $pictures->filename          = $filename;
                $pictures->original_filename = $this->imageFile->baseName . '.' . $this->imageFile->extension;
                $pictures->date_created      = new Expression('NOW()');
                $pictures->date_updated      = new Expression('NOW()');
                $pictures->save();
            }
            return true;
        } else {
            return false;
        }
    }
}